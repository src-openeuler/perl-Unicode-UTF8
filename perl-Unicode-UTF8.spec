%bcond_with perl_Unicode_UTF8_enables_Module_Install_ReadmeFromPod

Summary:    Encoding and decoding of UTF-8 encoding form
Name:       perl-Unicode-UTF8
Version:    0.62
Release:    9
License:    GPL-1.0-or-later OR Artistic-1.0-Perl
URL:        https://metacpan.org/release/Unicode-UTF8
Source0:    https://cpan.metacpan.org/authors/id/C/CH/CHANSEN/Unicode-UTF8-%{version}.tar.gz

BuildRequires:  coreutils findutils gcc make perl-interpreter perl-devel perl-generators
BuildRequires:  perl(Carp) perl(Exporter) perl(strict) perl(warnings) perl(XSLoader)
BuildRequires:  perl(Encode) >= 1.9801 perl(IO::File) perl(lib) perl(Scalar::Util)
BuildRequires:  perl(Test::Builder) perl(Test::More) >= 0.47
%if %{with perl_Unicode_UTF8_enables_Module_Install_ReadmeFromPod}
BuildRequires:  perl(inc::Module::Install) perl(Module::Install::ReadmeFromPod)
%else
BuildRequires:  perl(base) perl(Config) perl(Cwd) perl(ExtUtils::MakeMaker) perl(Fcntl)
BuildRequires:  perl(File::Basename) perl(File::Find) perl(File::Path) perl(Pod::Text) perl(vars)
%endif
Requires:   perl(Exporter)
Requires:   perl(XSLoader)

%{?perl_default_filter}

%description
The package is a module which provides functions to dencode and ecode UTF-8 encoding.

%package_help

%prep
%autosetup -n Unicode-UTF8-%{version}
%if %{with perl_Unicode_UTF8_enables_Module_Install_ReadmeFromPod}
rm -rf inc/
%endif

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%{make_build}

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%files
%{perl_vendorarch}/Unicode/
%{perl_vendorarch}/auto/Unicode/

%files help
%doc Changes README
%{_mandir}/man3/Unicode::UTF8.3*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.62-9
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.62-8
- Package init
